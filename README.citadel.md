This repo was set up using the instructions at https://gist.github.com/sangeeths/9467061
You should have two remotes
> git remote -v
> origin  git@bitbucket.org:andrewlorien_citadel/bash-my-aws-citadel.git (fetch)
> origin  git@bitbucket.org:andrewlorien_citadel/bash-my-aws-citadel.git (push)
> sync    https://github.com/bash-my-aws/bash-my-aws.git (fetch)
> sync    https://github.com/bash-my-aws/bash-my-aws.git (push)

To sync to the original github:

> # pull the github master
> git pull sync master --allow-unrelated-histories
> # add a file 
> git add lib/stack-functions
> git commit -m "add stack-detect-drift function"
> # push your new local file and the remote updates to citadel bitbucket
> git push -u origin master

